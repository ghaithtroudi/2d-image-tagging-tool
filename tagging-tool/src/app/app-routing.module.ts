import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { TagComponent } from './tag/tag.component';

const routes: Routes = [
    {
      path: 'tag',
      component: TagComponent
    },
    {
      path: '',
      pathMatch: 'full',
      component: HomeComponent,
    },
    {
      path: '**',
      component: NotFoundComponent,
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

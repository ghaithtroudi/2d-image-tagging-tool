import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Annotation } from 'src/app/models/annotation';
import { Frame } from 'src/app/models/frame';
import { TagCollection } from 'src/app/models/tag-collection';
import { FrameService } from 'src/app/services/frame/frame.service';
import { TagService } from 'src/app/services/tag/tag.service';

@Component({
  selector: 'app-frame-tags',
  templateUrl: './frame-tags.component.html',
  styleUrls: ['./frame-tags.component.css']
})
export class FrameTagsComponent implements OnInit, OnDestroy {

  tagsSubscription: Subscription;
  frameSubscription: Subscription;

  frame: Frame = null;
  tagCollections: TagCollection[] = [];
  selectedAnnotations: Annotation[] = [];
  cannotSave: boolean = true; // controls save button disabled attribute
  displayMessage: string = 'No Frame is selected'; // displays text to orient tagger


  constructor(
    private tagService: TagService,
    private frameService: FrameService
  ) { }

  ngOnInit(): void {
    this.tagCollections = this.tagService.getTags();
    this.tagsSubscription = this.tagService.newTagCollection.subscribe(
      collection => {
        this.tagCollections = collection;
      }
    );

    this.frameSubscription = this.frameService.newActiveFrame.subscribe(
      frame => {
        this.frame = frame;
        if (this.frame) {
          this.selectedAnnotations = [...this.frame.annotations];
          this.displayMessage = 'Please fill required tags';
          this.checkSaveFrameTags();
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.tagsSubscription.unsubscribe();
    this.frameSubscription.unsubscribe();
  }

  /**
   * Checks whether a tag is applied to a frame
   * @param type {string}
   * @param value {number}
   * @returns  boolean showing whether a given tag is selected
   */
  isSelected(type: string, value: number): boolean {
    const annotation = this.selectedAnnotations.find( a => a.type === type );
    if (!annotation) {
      return false;
    } else {
      return annotation.values.indexOf(value) >= 0;
    }
  }

  /**
   * Checks whether a frame tags cannot be saved and updates cannotSave value
   */
    checkSaveFrameTags(): void {
    this.cannotSave = false;
    const checkTagTypes = this.tagCollections.filter( coll => coll.required ).map( coll => coll.value );
    checkTagTypes.forEach( type => {
      const annotation = this.selectedAnnotations.find( a => a.type === type )
      if (!annotation || !annotation.values || annotation.values.length < 1) {
        this.cannotSave = true;
      }
    });
    if (!this.cannotSave) {
      this.displayMessage = '';
    }
  }

  /**
   * fired when a tag is clicked to link it to frame annotations
   * @param type string - type of the selected tag
   * @param value number - value of the tag selected
   */
  tagSelected(type:string, value: number): void {
    if (!this.frame) return;
    const collection = this.tagCollections.find( coll => coll.value === type );
    if (!collection) {
      return;
    }
    const annotation = this.selectedAnnotations.find( a => a.type === type );
    if (annotation) {
      if (annotation.values.indexOf(value) < 0) {
        // Special treatment for 0 ( Unclear or False )
        if (value === 0) {
          annotation.values = [value];
        } else {
          // remove Unclear if we use another value
          const unclearIndex = annotation.values.indexOf(0);
          if (unclearIndex >= 0) {
            annotation.values.splice(unclearIndex,1);
          }
          if (collection.multiple) {
            annotation.values.push(value);
          } else {
            annotation.values = [value];
          }
        }
      }
    } else {
      this.selectedAnnotations.push({ type, values: [value]});
    }
    this.checkSaveFrameTags();
  }

  /**
   * Saves selected annotations of the active frame by calling the FrameService
   * and updates displayed image annotations
   */
  saveAnnotations(): void {
    this.frameService.saveFrameAnnotations(this.frame.id, this.selectedAnnotations)
      .then(
        resp => {
            this.displayMessage = 'Saved Successfully';
        }
      ).catch(
        err => {
          this.displayMessage = err.msg;
        }
      );

  }


}

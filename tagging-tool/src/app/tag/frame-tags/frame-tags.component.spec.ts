import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { TagService } from 'src/app/services/tag/tag.service';

import { FrameTagsComponent } from './frame-tags.component';

describe('FrameTagsComponent', () => {
  let component: FrameTagsComponent;
  let fixture: ComponentFixture<FrameTagsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrameTagsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display text if no tags are given', () => {
    const fixture = TestBed.createComponent(FrameTagsComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Loading tags...');
  });

  it('should return false for selected tag', () => {
    const fixture = TestBed.createComponent(FrameTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.selectedAnnotations = [];
    expect( component.isSelected('weather', 2)).toBeFalse();
  });

  it('should return true for selected tag', () => {
    const fixture = TestBed.createComponent(FrameTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.selectedAnnotations = [{ type: 'weather', values: [2,5]}];
    expect( component.isSelected('weather', 2)).toBeTrue();
  });
});

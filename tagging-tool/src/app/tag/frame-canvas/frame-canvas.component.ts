import { Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Frame } from 'src/app/models/frame';
import { FrameService } from 'src/app/services/frame/frame.service';

@Component({
  selector: 'app-frame-canvas',
  templateUrl: './frame-canvas.component.html',
  styleUrls: ['./frame-canvas.component.css']
})
export class FrameCanvasComponent implements OnInit , OnDestroy{

  @ViewChild('img') imageElement: ElementRef;
  @ViewChild('lens') lensElement: ElementRef;
  @ViewChild('zoomedImg') zoomedElement: ElementRef;

  lensStyle = { };

  resultStyle = { }

  frame: Frame = null;
  frameSubscription: Subscription;

  constructor(private frameService: FrameService) { }

  ngOnInit(): void {
    this.frameSubscription = this.frameService.newActiveFrame.subscribe(
      newFrame => {
        this.frame = newFrame;
      }
    )
  }

  ngOnDestroy(): void {
    this.frameSubscription.unsubscribe();
  }

  /**
   * Executed when mouse on a Frame to display zoomed image
   * @param event HTTPEvent
   */
  zoomImage(event): void {
    event.preventDefault();
    event.stopPropagation();
    const cx =   this.zoomedElement.nativeElement.offsetWidth / this.lensElement.nativeElement.offsetWidth;
    const cy =   this.zoomedElement.nativeElement.offsetHeight / this.lensElement.nativeElement.offsetHeight;
    this.resultStyle['display'] = 'block';
    this.resultStyle['background-image'] = `url('${this.frame.path}')`;
    this.resultStyle['background-size'] = `${this.imageElement.nativeElement.width * cx}px ${this.imageElement.nativeElement.height * cy}px`;
    /* Calculate the position of the lens: */
    let x = event.layerX - ( this.lensElement.nativeElement.offsetWidth / 2);
    let y = event.layerY - ( this.lensElement.nativeElement.offsetHeight / 2);
    /* Prevent the lens from being positioned outside the image: */
    if (x > this.imageElement.nativeElement.width - this.lensElement.nativeElement.offsetWidth) {
      x = this.imageElement.nativeElement.width - this.lensElement.nativeElement.offsetWidth;
    }
    if (x < 0) {
      x = 0;
    }
    if (y > this.imageElement.nativeElement.height - this.lensElement.nativeElement.offsetHeight) {
      y = this.imageElement.nativeElement.height - this.lensElement.nativeElement.offsetHeight;
    }
    if (y < 0) {
      y = 0;
    }
    this.resultStyle['background-position'] = "-" + (x * cx) + "px -" + (y * cy) + "px";
    /* Set the position of the lens: */
    this.lensStyle['display'] = 'block';
    this.lensStyle['top'] = y + 'px';
    this.lensStyle['left'] = x + 'px';
  }

  /**
   * Executed when mouse quits frame canvas to hide zoomed image
   */
  leaveImage(): void {
    this.resultStyle['display'] = 'none';
    this.lensStyle['display'] = 'none';
  }
}

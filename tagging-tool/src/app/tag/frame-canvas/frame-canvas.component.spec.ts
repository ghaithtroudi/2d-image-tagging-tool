import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameCanvasComponent } from './frame-canvas.component';

describe('FrameCanvasComponent', () => {
  let component: FrameCanvasComponent;
  let fixture: ComponentFixture<FrameCanvasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrameCanvasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameCanvasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display text if no frame is given', () => {
    const fixture = TestBed.createComponent(FrameCanvasComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Please select a frame from the list');
  });

});

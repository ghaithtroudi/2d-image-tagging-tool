import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Frame } from 'src/app/models/frame';
import { FrameService } from 'src/app/services/frame/frame.service';

@Component({
  selector: 'app-frame-list',
  templateUrl: './frame-list.component.html',
  styleUrls: ['./frame-list.component.css']
})
export class FrameListComponent implements OnInit, OnDestroy {

  frameListSubscription: Subscription;
  newFrameSubscription: Subscription;
  frames: Frame[] = [];
  activeFrame: Frame = null;

  constructor(
    private frameService: FrameService
    ) { }

  ngOnInit(): void {
    this.frames = this.frameService.getFrames();
    this.frameListSubscription = this.frameService.newFrameList.subscribe(
      frames => {
        this.frames = frames
      }
    );
    this.newFrameSubscription = this.frameService.newActiveFrame.subscribe(
      frame => {
        this.activeFrame = frame;
      }
    )
  }

  ngOnDestroy(): void {
    this.frameListSubscription.unsubscribe();
  }

  /**
   * Executed when a frame is clicked to setFrame method on frame service
   */
  selectedFrame(id: number): void {
    this.frameService.setFrame(id);
  }

  /**
   * Checks whether a given frame is selected using it's id
   * @param id number - id of the frame
   * @returns true if a given frame is active
   */
  isActive(id: number): boolean {
    return this.activeFrame && this.activeFrame.id === id;
  }

}

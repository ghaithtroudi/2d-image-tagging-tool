import { Component, OnInit } from '@angular/core';
import { FrameService } from '../services/frame/frame.service';
import { TagService } from '../services/tag/tag.service';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

  constructor(
    private frameService: FrameService,
    private tagService: TagService
  ) { }

  ngOnInit(): void {
    // these functions are called to simulate API calls
    this.frameService.loadFrames('/assets/data/frames.json');
    // this.frameService.loadFrames('/assets/data/frames-prefilled.json');
    this.tagService.loadTags('/assets/data/tags.json')
  }

}

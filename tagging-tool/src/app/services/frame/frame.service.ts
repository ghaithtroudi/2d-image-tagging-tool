import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Annotation } from 'src/app/models/annotation';
import { Frame } from 'src/app/models/frame';
import { HttpEmulatorService } from '../http-emulator/http-emulator.service';

@Injectable({
  providedIn: 'root'
})
export class FrameService {

  frames: Frame[] = [];
  activeFrame: Frame = null;

  newFrameList: Subject<Frame[]> = new Subject<Frame[]>();
  newActiveFrame: Subject<Frame> = new Subject<Frame>();

  constructor(
    private httpEmulator: HttpEmulatorService
  ) {}

  /**
   * Loads the list of frames and sends a subscribe event on change
   * @param path string - path of the requested frames list
   * @returns emits a FrameList
   */
   loadFrames(path: string): void {
    this.httpEmulator.get(path)
    .then( res => {
      this.frames = res as Frame[];
      this.activeFrame = null;
      this.newActiveFrame.next(this.activeFrame);
      this.newFrameList.next(this.frames);
    })
    .catch( error => {
      console.error(error);
      this.frames = [];
      this.newFrameList.next([]);
    });
  }

  /**
   * Get the list of frames in Memory synchronously
   * @returns Frame[]
   */
  getFrames(): Frame[] {
    return this.frames;
  }

  /**
   * Get the active Frame synchronously
   * @returns {Frame}
   */
    getActiveFrame(): Frame {
    return this.activeFrame;
  }

  /**
   * Sets the frame
   * @param id number - id of the Frame to view
   */
  setFrame(id: number): void {
    if (!this.activeFrame || this.activeFrame.id !== id ) { // No need to emit an event if it is the same frame
      const selectedFrame = this.frames.find( x => x.id === id);
      this.newActiveFrame.next(selectedFrame || null);
    }
  }

  /**
   * Saves frame annotations
   * @param id number - id of the frame to save
   * @param annotations Annotation[] - list of the annotations to save
   * @returns Promise<boolean> - to show whether the frame is successfully saved
   */
  saveFrameAnnotations(id: number, annotations: Annotation[]): Promise<boolean> {
    // Save using a service at a later time
    return new Promise( (resolve, reject) => {
      const frame = this.frames.find( f => f.id === id )
      if (frame) {
        frame.annotations = annotations;
        resolve(true);
      } else {
        reject({msg: 'Frame not found'});
      }
    });
}


}

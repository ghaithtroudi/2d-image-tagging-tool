import { TestBed } from '@angular/core/testing';
import { Frame } from 'src/app/models/frame';

import { FrameService } from './frame.service';

describe('FrameService', () => {
  let service: FrameService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FrameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get subscribed Frame list', () => {
    const frames: Frame[] = [
      {
        "id": 0,
        "thumbnail": "https://upload.wikimedia.org/wikipedia/commons/thumb/6/68/St_Kilda_Road_start.jpg/320px-St_Kilda_Road_start.jpg",
        "path": "https://upload.wikimedia.org/wikipedia/commons/6/68/St_Kilda_Road_start.jpg",
        "annotations": [],
        "details": "Credits to Vincent Quach from <a href=\"https://commons.wikimedia.org/wiki/File:St_Kilda_Road_start.jpg\" target=\"_blank\">Wikimedia</a> under Creative Commons 3.0 licence"
      }
    ]
    const fixture = TestBed.inject(FrameService);
    fixture.newFrameList.subscribe(
      value => {
        expect(value).toEqual(frames);
      }
     );
    fixture.newFrameList.next(frames);
  });

  it('should get subscribed Frame', () => {
    const frame: Frame = {
      "id": 0,
      "thumbnail": "thumbnail",
      "path": "path",
      "annotations": [],
      "details": "details"
    };
    let fixture = TestBed.inject(FrameService);
    fixture.newActiveFrame.subscribe(
      value => {
        expect(value).toEqual(frame);
      }
     );
    fixture.newActiveFrame.next(frame);
  });

  it('should emit null when no frame is found', () => {
    let fixture = TestBed.inject(FrameService);
    fixture.newActiveFrame.subscribe(
      frame => {
        expect(frame).toBeNull();
      }
    )
    fixture.setFrame(2);
  });
});

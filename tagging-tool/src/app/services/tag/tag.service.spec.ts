import { TestBed } from '@angular/core/testing';
import { TagCollection } from 'src/app/models/tag-collection';

import { TagService } from './tag.service';

describe('TagService', () => {
  let service: TagService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TagService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get subscribed TagCollection', () => {
    const collection: TagCollection[] = [
      {
        "name": "Lane count",
        "value": "lane_count",
        "multiple": false,
        "required": true,
        "options": [
          {
            "label": "1",
            "value": 1
          },
          {
            "label": "2",
            "value": 2
          },
          {
            "label": "3",
            "value": 3
          },
          {
            "label": "4",
            "value": 4
          },
          {
            "label": "5",
            "value": 5
          },
          {
            "label": "6",
            "value": 6
          },
          {
            "label": "7",
            "value": 7
          },
          {
            "label": "8",
            "value": 8
          },
          {
            "label": "Unclear",
            "value": 0
          }
        ]
      }
    ]
    const fixture = TestBed.inject(TagService);
    fixture.newTagCollection.subscribe(
      value => {
        expect(value).toEqual(collection);
      }
      );
    fixture.newTagCollection.next(collection);
    });
});

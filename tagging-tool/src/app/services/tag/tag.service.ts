import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { TagCollection } from '../../models/tag-collection';
import { HttpEmulatorService } from '../http-emulator/http-emulator.service';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  /* List of tags to be loaded once and keep in memory */
  tagsCollection: TagCollection[] = [];
  newTagCollection: Subject<TagCollection[]> = new Subject<TagCollection[]>();

  constructor(private httpEmulator: HttpEmulatorService) {}

  /**
   * Returns the list of available tags for the labeller
   * @param path string - path of the requested tags collection
   * @returns emits a TagCollection array or an empty array
   */
  loadTags(path: string): void {
    this.httpEmulator.get(path)
    .then( res => {
      this.tagsCollection = res as TagCollection[];
      this.newTagCollection.next(this.tagsCollection);
    })
    .catch( error => {
        console.error(error);
        this.newTagCollection.next(this.tagsCollection);
    });
  }

  /**
   * Get a reference on the TagCollection array
   * @returns TagCollection[]
   */
  getTags(): TagCollection[] {
    return this.tagsCollection;
  }

}

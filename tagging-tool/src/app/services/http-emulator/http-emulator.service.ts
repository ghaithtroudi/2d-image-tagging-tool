import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HttpEmulatorService {

  constructor() { }

  /**
   * Returns a promess of a given json path as an emulation of a http get promise
   * @param path {string} - path of the get request
   * @returns object|object[] - the content of a json file
   * @throws Error that will be propagated to the caller function
   */
   get(path: string) {
    return fetch(path)
    .then(
      (resp) => {
        if (resp.ok) {
          return resp.text();
        } else {
          throw resp;
        }
      }
    )
    .then(
      text => {
        return JSON.parse(text);
      }
    )
  }

}

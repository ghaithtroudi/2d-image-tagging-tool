import {  TestBed, waitForAsync } from '@angular/core/testing';

import { HttpEmulatorService } from './http-emulator.service';

describe('HttpEmulatorService', () => {
  let service: HttpEmulatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpEmulatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return async promise on get Request', waitForAsync(() => {
    const dummy = { type: 'dummy', msg: 'Dummy data'};
    let fixture = TestBed.inject(HttpEmulatorService);
    let spy = spyOn(fixture, 'get').and.returnValue(Promise.resolve(dummy));
    fixture.get('path').then(
      resp =>
      {
        expect(resp).toEqual(dummy);
      }
    )

  }));
});

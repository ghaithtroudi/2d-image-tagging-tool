import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TagComponent } from './tag/tag.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { FrameCanvasComponent } from './tag/frame-canvas/frame-canvas.component';
import { FrameTagsComponent } from './tag/frame-tags/frame-tags.component';
import { FrameListComponent } from './tag/frame-list/frame-list.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TagComponent,
    NotFoundComponent,
    FrameCanvasComponent,
    FrameTagsComponent,
    FrameListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

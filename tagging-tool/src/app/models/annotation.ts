export class Annotation {
  constructor(
    public type: string,
    public values: number[]
  ) {}
}

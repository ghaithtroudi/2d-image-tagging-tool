export class Tag {
  constructor(
    public label: string,
    public value: number,
    public selected?: boolean
  ) {}
}

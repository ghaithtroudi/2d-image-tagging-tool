import { Tag } from './tag';

export class TagCollection {
  constructor(
    public name: string,
    public value: string,
    public multiple: boolean,
    public required: boolean,
    public options: Tag[]
  ) {};
}

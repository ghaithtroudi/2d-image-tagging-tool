import { Annotation } from "./annotation";

export class Frame {
  constructor(
    public id: number,
    public thumbnail: string, // thumbnail url path
    public path: string, // image url path
    public annotations: Annotation[],
    public details?: string // html description
  ) {}
}

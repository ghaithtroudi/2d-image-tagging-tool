# 2D Image Tagging Tool

Image Tagging tool is a simple annotation tool for image frames.

## Version
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.5.

## Installation
A [node.js](https://nodejs.org) environment is needed to run this solution

Use Node Package manager [npm](https://pip.pypa.io/en/stable/) to install dependencies.

```bash
npm install
```

## Run solution
Use angular cli to serve solution on [localhost:4200](http://localhost:4200)

```bash
ng serve
```
## Testing
Use angular cli to run unit tests

```bash
ng test
```
